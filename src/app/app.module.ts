import { BlogPostsService } from './shered/services/blog-posts.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MaterialModule } from './material.module';
import { LogInComponent } from './log-in/log-in.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { AuthService } from './shered/services/auth.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from "@angular/fire/auth";
import { environment } from 'src/environments/environment';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AppRoutingModule } from './app-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ClassifiedComponent } from './classified/classified.component';
import { ClassifyFormComponent } from './classify-form/classify-form.component';
import { FormsModule } from '@angular/forms';
import { BlogPostsComponent } from './blog-posts/blog-posts.component';
import { CommentsComponent } from './comments/comments.component';
import { PostCommentsService } from './shered/services/post-comments.service';
import { SavedPostsComponent } from './saved-posts/saved-posts.component';


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    LogInComponent,
    ForgotPasswordComponent,
    SignUpComponent,
    VerifyEmailComponent,
    ErrorPageComponent,
    DashboardComponent,
    ClassifiedComponent,
    ClassifyFormComponent,
    BlogPostsComponent,
    CommentsComponent,
    SavedPostsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MaterialModule,
    AppRoutingModule,
    FlexLayoutModule,
    HttpClientModule,
    AngularFireModule,
    AngularFirestoreModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    FormsModule
   
  ],
  providers: [AuthService, BlogPostsService,PostCommentsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
