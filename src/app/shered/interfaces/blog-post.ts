export interface BlogPost {
    id:number,
    userId: number,
    title: string,
    body: string,
}
