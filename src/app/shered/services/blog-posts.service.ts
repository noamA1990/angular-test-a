import { BlogPost } from './../interfaces/blog-post';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { NotificationService } from './notification.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BlogPostsService {
  
  private POSTURL = "https://jsonplaceholder.typicode.com/posts";
  private SINGLEPOSTURL = "https://jsonplaceholder.typicode.com/posts/";
  private COMMENTURL = "https://jsonplaceholder.typicode.com/comments?postId=";
  

  private itemsCollection: AngularFirestoreCollection<BlogPost>;
  userCollection:AngularFirestoreCollection = this.db.collection('users');
  postsCollection:AngularFirestoreCollection

  constructor(private http: HttpClient, private db: AngularFirestore,
    private notificationService: NotificationService){
    
}


getPosts(): Observable<BlogPost[]>{
  return this.http.get<BlogPost[]>(`${this.POSTURL}`);
}


getSinglePost(id:number): Observable<BlogPost>{
  return this.http.get<BlogPost>(`${this.SINGLEPOSTURL}${id}`);
}

getComments(postId:number): Observable<Comment>{
  console.log("URL: " ,`${this.COMMENTURL}${postId}`)
  return this.http.get<Comment>(`${this.COMMENTURL}${postId}`);
}

// getPostFromDB():Observable<BlogPost[]>{
//     this.postsCollection = this.itemsCollection.valueChanges({idField: 'key'});
//     return this.postsCollection;
// }
saveToFirebase(id, uid, title, body){
  this.userCollection.doc(uid).collection('posts').add({
    id: id,
    userId: uid,
    title: title,
    body: body
  })
}

getUserPosts(userId): Observable<any[]> {
  //const ref = this.db.collection('books');
  //return ref.valueChanges({idField: 'id'});
  this.postsCollection = this.db.collection(`users/${userId}/posts`);
  return this.postsCollection.snapshotChanges().pipe(
    map(collection => collection.map(document => {
      const data = document.payload.doc.data();
      data.id = document.payload.doc.id;

      return data;
    }))
  );    
} 

deletePost(userId:string, id:string){
  this.db.doc(`users/${userId}/posts/${id}`).delete();
}

addLikes(userId:string, pid:string, likes:number){
  console.log(likes)
  const like = likes + 1;
    this.db.doc(`users/${userId}/posts/${pid}`).update(
      {
        likes: like
      }
    )
  
}
}
