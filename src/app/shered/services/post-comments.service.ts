import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Comment } from '../interfaces/comment';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostCommentsService {

  COMMENTURL = "https://jsonplaceholder.typicode.com/comments?postId=";

  constructor(private http: HttpClient, private db: AngularFirestore) { }

  getComments(postId): Observable<Comment[]>{
    console.log("URL: " ,`${this.COMMENTURL}${postId}`)
    return this.http.get<Comment[]>(`${this.COMMENTURL}${postId}`)
  }

}
