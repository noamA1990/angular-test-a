import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ClassifyService {
  private url = " https://famdojykcb.execute-api.us-east-1.amazonaws.com/beta";
  
  public categories:object = {0: 'business', 1: 'entertainment', 2: 'politics', 3: 'sport', 4: 'tech'}
  
  public doc:string; 
  
  classify():Observable<any>{
    console.log(this.doc);
    let json = {
      "articles": [
        {
          "text": this.doc
        },
      ]
    }
    let body  = JSON.stringify(json);
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log(res);
        console.log(res.body);
        let final = res.body.replace('[','');
        final = final.replace(']','');
        console.log(final);
        return final;      
      })
    );      
  }

  constructor(private http: HttpClient) { }
}
