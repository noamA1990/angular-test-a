import { Component, OnInit } from '@angular/core';
import { BlogPostsService } from '../shered/services/blog-posts.service';
import { AuthService } from '../shered/services/auth.service';
import { Observable } from 'rxjs';
import { BlogPost } from '../shered/interfaces/blog-post';

@Component({
  selector: 'app-saved-posts',
  templateUrl: './saved-posts.component.html',
  styleUrls: ['./saved-posts.component.css']
})
export class SavedPostsComponent implements OnInit {
  
  dbPosts$: Observable<BlogPost[]>;

  constructor(private blogpostsService: BlogPostsService,public auth:AuthService) { }

  ngOnInit() {
    
    this.dbPosts$ = this.blogpostsService.getUserPosts(this.auth.getUser().uid);
    
  }

  deletePost(postId){
    this.blogpostsService.deletePost(this.auth.getUser().uid, postId)
  }

  addLikes(pid, likes){
    this.blogpostsService.addLikes(this.auth.getUser().uid,pid, likes);
  }

}
