import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../shered/services/classify.service';
import { ImageService } from '../shered/services/image.service';

@Component({
  selector: 'app-classified',
  templateUrl: './classified.component.html',
  styleUrls: ['./classified.component.css']
})
export class ClassifiedComponent implements OnInit {

  category:string = "Loading...";
  categoryImage:string; 
  constructor(public classifyService:ClassifyService,
    public imageService:ImageService) { }

  ngOnInit() {
    this.classifyService.classify().subscribe(
      res => {
        this.category = this.classifyService.categories[res];
        this.categoryImage = this.imageService.images[res];
      }
    )
  }

}
