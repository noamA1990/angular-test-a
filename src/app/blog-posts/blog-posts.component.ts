import { BlogPostsService } from './../shered/services/blog-posts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { BlogPost } from '../shered/interfaces/blog-post';
import { NotificationService } from '../shered/services/notification.service';
import { AuthService } from '../shered/services/auth.service';

@Component({
  selector: 'app-blog-posts',
  templateUrl: './blog-posts.component.html',
  styleUrls: ['./blog-posts.component.css']
})
export class BlogPostsComponent implements OnInit {

  panelOpenState = false;
  blogPosts$:Observable<BlogPost[]>;
  post$: Observable<BlogPost>;
  message: string;
  // dbPosts$: Observable<BlogPost[]>;


   constructor(private blogpostsService: BlogPostsService,private notificationService: NotificationService,public auth:AuthService) { }

  ngOnInit() {
    this.blogPosts$ = this.blogpostsService.getPosts();
    // this.dbPosts$ = this.blogpostsService.getUserPosts(this.auth.getUser().uid);
  }

  save(id: number){
    this.post$ = this.blogpostsService.getSinglePost(id);
    this.post$.subscribe(post => {
      this.blogpostsService.saveToFirebase(post.id,this.auth.getUser().uid,post.title,post.body);
    })

    this.message = "The post added successfully!";
    this.notificationService.success(this.message);
    
    
    
  }
}
