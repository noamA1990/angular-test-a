import { CommentsComponent } from './comments/comments.component';
import { BlogPostsComponent } from './blog-posts/blog-posts.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogInComponent } from './log-in/log-in.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { SecureInnerPagesGuard } from './shered/gurad/secure-inner-pages.guard';
import { AuthGuard } from './shered/gurad/auth.guard';
import { ErrorPageComponent } from './error-page/error-page.component';

import { SavedPostsComponent } from './saved-posts/saved-posts.component';

const appRoutes: Routes =[
    { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
    { path: 'log-in', component: LogInComponent },
    { path: 'dashboard', component: DashboardComponent },
    { path: 'register-user', component: SignUpComponent, canActivate: [SecureInnerPagesGuard]},
    { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [SecureInnerPagesGuard] },
    { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [SecureInnerPagesGuard] },

    { path: 'posts', children: [
        { path: '',  component: BlogPostsComponent, canActivate: [AuthGuard] },
        { path: ':postId/comments', component: CommentsComponent, canActivate: [AuthGuard] },
    ]
    },
    { path: 'savePosts', component: SavedPostsComponent,canActivate: [AuthGuard]},


    { path: 'not-found', component:ErrorPageComponent, data: {message: 'Page not found!'} },
    { path: '**', redirectTo: '/not-found' }
];


@NgModule({
    imports: [
        // RouterModule.forRoot(appRoutes, {useHash :true})
        RouterModule.forRoot(appRoutes)

    ],
    exports: [RouterModule]
})

export class AppRoutingModule{}