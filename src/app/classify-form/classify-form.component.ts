import { Component, OnInit } from '@angular/core';
import { ClassifyService } from '../shered/services/classify.service';
import { ImageService } from '../shered/services/image.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-classify-form',
  templateUrl: './classify-form.component.html',
  styleUrls: ['./classify-form.component.css']
})
export class ClassifyFormComponent implements OnInit {

  url:string;
  text:string;

  constructor(private classifyService:ClassifyService, private router:Router) { }

  ngOnInit() {
  }

  onSubmit(){
    this.classifyService.doc = this.text;
    this.router.navigate(['/classified']);
  }

}
