import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shered/services/auth.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {

  userData: any;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.userData = this.authService.userData;
  }
  SendMail(){
    this.authService.SendVerificationMail();
  }
}
