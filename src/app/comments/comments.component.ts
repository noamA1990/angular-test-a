import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BlogPostsService } from '../shered/services/blog-posts.service';
import { Observable } from 'rxjs';
import { BlogPost } from '../shered/interfaces/blog-post';
import { PostCommentsService } from '../shered/services/post-comments.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css']
})
export class CommentsComponent implements OnInit {

  comments$:Observable<Comment>;
  postTitle;
  postId: number;
  post$: Observable<BlogPost>

  constructor(private router: Router, private route: ActivatedRoute, private blogpostsService: BlogPostsService,
              private postcommentService: PostCommentsService) { }

  ngOnInit() {
    this.postId = this.route.snapshot.params.postId;
    this.post$ = this.blogpostsService.getSinglePost(this.postId);
    this.post$.subscribe(post => {
      this.postTitle = post.title
    })
    console.log("id: ", this.postId)
    this.comments$ = this.blogpostsService.getComments(this.postId);
    
   
  }
}
