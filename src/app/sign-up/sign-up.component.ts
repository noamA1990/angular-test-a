import { Component, OnInit } from '@angular/core';
import { AuthService } from '../shered/services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(private authService: AuthService) { }

  ngOnInit() {
  }

  SignUp(name, pass){
    this.authService.SignUp(name, pass);
  }
}
