// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBQlCCHmwCHYRfhUAEXSRGROknTPhZYTlE",
    authDomain: "angular-test-a-36d98.firebaseapp.com",
    databaseURL: "https://angular-test-a-36d98.firebaseio.com",
    projectId: "angular-test-a-36d98",
    storageBucket: "angular-test-a-36d98.appspot.com",
    messagingSenderId: "251835764324",
    appId: "1:251835764324:web:2e40fe15c53acb63685971"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
